#!/bin/bash

function create_patch(){
    cat <<EOT >> compose.patch
--- docker-compose.yml	2021-10-14 17:55:27.839678685 -0300
+++ docker-compose.yaml	2021-10-14 17:55:31.482984707 -0300
@@ -2,6 +2,7 @@
 services:
   postgresql:
     image: docker.io/bitnami/postgresql:13
+    restart: always
     volumes:
       - 'postgresql_data:/bitnami/postgresql'
     environment:
@@ -11,8 +12,9 @@
       - POSTGRESQL_DATABASE=bitnami_sonarqube
   sonarqube:
     image: docker.io/bitnami/sonarqube:9
+    restart: always
     ports:
-      - '80:9000'
+      - '9000:9000'
     volumes:
       - 'sonarqube_data:/bitnami/sonarqube'
     depends_on:
EOT
}

function sonacube_docker(){
    cd ~/. 
    mkdir sonarcube
    cd sonarcube/
    FILE=./docker-compose.yml
    if [ ! -f "$FILE" ]; then
      curl -sSL https://raw.githubusercontent.com/bitnami/bitnami-docker-sonarqube/master/docker-compose.yml -o docker-compose.yml
      create_patch
      patch docker-compose.yml compose.patch 
    fi
  
    sudo sysctl -w vm.max_map_count=262144
    sudo docker-compose up -d
}

function sonar_scanner(){
    wget https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-4.6.2.2472-linux.zip
    unzip sonar-scanner-cli-4.6.2.2472-linux.zip
    echo "export PATH=$(pwd)/sonar-scanner-4.6.2.2472-linux/bin:$PATH" >> ~/.bashrc
    source ~/.bashrc
    return
}

function sonar_report() {
    sudo apt install npm -y
    sudo npm install -g sonar-report    
}

function owasp_zap() {
    sudo docker pull owasp/zap2docker-stable
}

sonacube_docker
sonar_scanner
sonar_report
owasp_zap