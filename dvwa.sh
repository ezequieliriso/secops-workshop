#!/bin/bash

running=$(sudo docker ps -a | grep dvwa)

if [ -z "$running" ]
then
      sudo docker run -d --restart always --name dvwa -p 8081:80 vulnerables/web-dvwa
else
      sudo docker start dvwa
fi