#!/bin/bash

sudo systemctl restart jenkins
cd ~/
cd sonarcube/
sudo docker-compose up -d
cd ..
cd django-DefectDojo
sudo docker-compose up -d
dojo_password=$(docker-compose logs initializer | grep "Admin password:" | awk '{print $5}')
cd ..

ip4=$(/sbin/ip -o -4 addr list enp0s8 | awk '{print $4}' | cut -d/ -f1)
echo "Jenkins Password "$(sudo cat /var/lib/jenkins/secrets/initialAdminPassword)
echo "Sonarcube: http://$ip4:9000 admin:bitnami"
echo "Dojo: http://$ip4:8082 admin:$dojo_password"
echo "dvwa: http://$ip4:8081 admin:password"
