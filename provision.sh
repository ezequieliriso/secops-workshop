#!/bin/bash

function update() {
  sudo apt update
  DEBIAN_FRONTEND=noninteractive sudo apt-get -y -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" upgrade
  echo "instance Updated"
}

function dependencies() {
  update
  sudo apt install sudo default-jre perl unzip libjson-perl libwww-perl htop python make wget git rdiff-backup rsync socat iptables screen net-tools -y
}


function docker_install() {
    sudo apt update
    sudo apt install apt-transport-https ca-certificates curl software-properties-common -y
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    echo "deb [arch=amd64] https://download.docker.com/linux/ubuntu hirsute stable" >> /etc/apt/sources.list.d/docker.list
    sudo apt update 
    sudo apt install docker-ce docker-compose -y
    sudo usermod -aG docker vagrant
    sudo usermod -aG docker jenkins
}

function jenkins_install() {
    wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo apt-key add -
    sh -c 'echo deb https://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'
    sudo apt update
    sudo apt install jenkins -y
    echo "Jenkins Password "$(sudo cat /var/lib/jenkins/secrets/initialAdminPassword)
    echo
}

dependencies
jenkins_install
docker_install
